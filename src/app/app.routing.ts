import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import {WelcomePageComponent} from './welcome-page/welcome-page.component';
import {PublicModule} from './public/public.module';


const routes: Routes = [
    {
        path: 'public',
        loadChildren: './public/public.module#PublicModule',
    },
    { path: 'Welcome', component: WelcomePageComponent },
    { path: '**', redirectTo: 'Welcome', pathMatch: 'full' },
];

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        RouterModule.forRoot(routes)
    ],
    exports: [
    ],
})
export class AppRoutingModule { }
