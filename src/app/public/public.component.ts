import { Component, OnInit, Inject, Renderer, ElementRef, ViewChild } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/filter';
import { DOCUMENT } from '@angular/platform-browser';
import { LocationStrategy, PlatformLocation, Location } from '@angular/common';
import { NavbarComponent } from '../shared/navbar/navbar.component';

@Component({
  selector: 'app-public',
  templateUrl: './public.component.html'
})
export class PublicComponent implements  OnInit {
    private _router: Subscription;
    @ViewChild(NavbarComponent) navbar: NavbarComponent;

    constructor( private renderer : Renderer, private router: Router, @Inject(DOCUMENT,) private document: any, private element : ElementRef, public location: Location) {}
    ngOnInit() {
        const navbar: HTMLElement = this.element.nativeElement.children[0].children[0];
        navbar.classList.remove('navbar-transparent');
        console.log(navbar);
        // this.renderer.listenGlobal('window', 'scroll', (event) => {
        //     const number = window.scrollY;
        //     let _location = this.location.path();
        //     _location = _location.split('/')[2];
        //
        //     if (number > 150 || window.pageYOffset > 150) {
        //         navbar.classList.remove('navbar-transparent');
        //     } else  {
        //         // remove logic
        //         navbar.classList.add('navbar-transparent');
        //     }
        // });

    }
}
