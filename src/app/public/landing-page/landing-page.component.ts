import { Component, OnInit, Renderer, OnDestroy } from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { NgbAccordionConfig } from '@ng-bootstrap/ng-bootstrap';
import * as Rellax from 'rellax';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements  OnInit, OnDestroy {


    imageUrls: (string | IImage)[] = [
        'assets/img/1.jpg',
        'assets/img/1.jpg',
        'assets/img/1.jpg',
        'assets/img/1.jpg',
        'assets/img/1.jpg',
        'assets/img/1.jpg',




    ];
    height = '100%';
    minHeight: string;
    arrowSize = '30px';
    showArrows = true;
    disableSwiping = false;
    autoPlay = true;
    autoPlayInterval = 3333;
    stopAutoPlayOnSlide = true;
    debug = false;
    backgroundSize = 'cover';
    backgroundPosition = 'center center';
    backgroundRepeat = 'no-repeat';
    showDots = true;
    dotColor = '#FFF';
    showCaptions = true;
    captionColor = '#FFF';
    captionBackground = 'rgba(0, 0, 0, .35)';
    lazyLoad = false;
    width = '100%';




    data: Date = new Date();

    page = 4;
    page1 = 5;
    page2 = 3;
    focus;
    focus1;
    focus2;

    date: {year: number, month: number};
    model: NgbDateStruct;

    public isCollapsed = true;
    public isCollapsed1 = true;
    public isCollapsed2 = true;

    state_icon_primary = true;

    constructor( private renderer: Renderer, config: NgbAccordionConfig) {
        config.closeOthers = true;
        config.type = 'info';
    }
    isWeekend(date: NgbDateStruct) {
        const d = new Date(date.year, date.month - 1, date.day);
        return d.getDay() === 0 || d.getDay() === 6;
    }

    isDisabled(date: NgbDateStruct, current: {month: number}) {
        return date.month !== current.month;
    }

    ngOnInit() {
        const rellaxHeader = new Rellax('.rellax-header');

        const navbar = document.getElementsByTagName('nav')[0];
        // navbar.classList.add('navbar-transparent');
        const body = document.getElementsByTagName('body')[0];
        body.classList.add('index-page');
    }
    ngOnDestroy() {
        const navbar = document.getElementsByTagName('nav')[0];
        // navbar.classList.remove('navbar-transparent');
        const body = document.getElementsByTagName('body')[0];
        body.classList.remove('index-page');
    }
}

export interface IImage {
    url: string | null;
    href?: string;
    clickAction?: Function;
    caption?: string;
    title?: string;
    backgroundSize?: string;
    backgroundPosition?: string;
    backgroundRepeat?: string;
}