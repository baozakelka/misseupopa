import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {PublicComponent} from './public.component';
import {LandingPageComponent} from './landing-page/landing-page.component';

const routes: Routes = [
    {
        path: '', component: PublicComponent,
        children: [
            {path: '', redirectTo: 'landingPage', pathMatch: 'full'},
            {path: 'landingPage', component: LandingPageComponent}
        ]
    }
];


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PublicRoutingModule {
}