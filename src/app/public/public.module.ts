import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { NouisliderModule } from 'ng2-nouislider';
import { JWBootstrapSwitchModule } from 'jw-bootstrap-switch-ng2';
import { RouterModule } from '@angular/router';
import { NavbarComponent } from '../shared/navbar/navbar.component';

import { LandingPageComponent } from './landing-page/landing-page.component';
import { PublicComponent } from './public.component';
import {SlideshowModule} from 'ng-simple-slideshow';
import { PublicRoutingModule } from './public-routing.module';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgbModule,
        NouisliderModule,
        RouterModule,
        JWBootstrapSwitchModule,
        PublicRoutingModule,
        SlideshowModule
      ],
    declarations: [
        LandingPageComponent,
        PublicComponent,
        NavbarComponent
    ],
    exports: []
})
export class PublicModule { }
