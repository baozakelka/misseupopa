import {Component, OnInit} from '@angular/core';
import {  Router } from '@angular/router';
@Component({
    selector: 'app-welcome-page',
    templateUrl: './welcome-page.component.html',
    styleUrls: ['./welcome-page.component.scss']
})
export class WelcomePageComponent implements OnInit {

    showPlayButton = false;
    constructor(  private _router: Router) {

    }

    goToLandingPage() {
        console.log('test');
        this._router.navigate(['/public']);
    }

    play(){
        this.showPlayButton = false;
        (<HTMLVideoElement>document.getElementById('myVideo')).play();
    }
    ngOnInit() {
        let promise = (<HTMLVideoElement>document.getElementById('myVideo'));
        promise.muted = true;
        if (promise.play() !== undefined) {
            promise.play().then(_ => {
                console.log('success')
            }).catch(error => {
                console.log('error',error);
                this.showPlayButton = true;
            });
        }
    }

}
