import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; // this is needed!
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app.routing';
import { PublicModule } from './public/public.module';


import { AppComponent } from './app.component';

import { WelcomePageComponent } from './welcome-page/welcome-page.component';

@NgModule({
    declarations: [
        AppComponent,

        WelcomePageComponent
    ],
    imports: [
        BrowserAnimationsModule,
        NgbModule.forRoot(),
        FormsModule,
        RouterModule,
        AppRoutingModule,
        PublicModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
